<%-- 
    Document   : login
    Created on : 28/05/2015, 16:37:14
    Author     : Gabriel Ferrari
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:useBean id="lbean" class="utfpr.ct.dainf.if6ae.exemplos.loginBean" scope="session"></jsp:useBean>
<jsp:setProperty property="*" name="lbean"/>
       
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title> 
    </head>
    <body> 
       <form method="post" action="/pratica-jsp/login.jsp">
         Código: <input type="text" name="login" /><br/>
         Nome: <input type="password" name="senha" /><br/>
         Perfil: <select name="perfil">
                     <option value="1">Cliente</option>
                     <option value="2">Gerente</option>
                     <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/>
      </form> 
      <jsp:setProperty property="login" name="lbean" /><br> 
      <jsp:setProperty property="senha" name="lbean"/><br> 
      <jsp:setProperty property="perfil" name="lbean" /><br>
      
      <%
          if ("POST".equalsIgnoreCase(request.getMethod())) {
                switch(lbean.getPerfil()){
                    case "1": 
                    lbean.setPerfil("Cliente");
                    break;
                    case "2":
                    lbean.setPerfil("Gerente");
                    break;
                    case "3":
                    lbean.setPerfil("Administrador");
                    break;
                }      
       
          if(lbean.getLogin().equals(lbean.getSenha())){
              %>
              <div  style="color:blue; font-style:italic">
                  <jsp:getProperty property="perfil" name="lbean" />, login bem sucedido, para <jsp:getProperty property="login" name="lbean" /> às
              <%
              out.print(lbean.getDate());
              %>
              </div>
              <%
          }
          
          else if(lbean.getLogin() == null || lbean.getSenha() == null){
              out.print("");
          }
          else{
              %>
              <div  style="color:red; font-style:italic">Acesso negado</div>
              <%
          }
    }     
//          
          %>
          
    </body>
    </html>
